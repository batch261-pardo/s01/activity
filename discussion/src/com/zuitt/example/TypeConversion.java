package com.zuitt.example;

import java.util.Scanner;
public class TypeConversion {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter numbers to add.");
        System.out.print("Enter a first number: ");

        //int num1 = Integer.parseInt(scan.nextLine());
        double num1 = scan.nextDouble();

        System.out.print("Enter second number: ");
        double num2 = scan.nextDouble();

        System.out.println("---------------------------------------------");
        System.out.print("The sum of two numbers is: " + (num1 + num2));

        //nextInt() - accepts Integer
        //nextDouble() - accepts Double/Float
        //nextLine() - accepts string input


    }
}
