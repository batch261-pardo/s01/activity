package com.zuitt.example;

import java.util.Scanner;
public class Activity {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        System.out.print("First Name: ");
        String firstName = scan.nextLine();

        System.out.print("Last Name: ");
        String lastName = scan.nextLine();

        System.out.print("First Subject Grade: ");
        double firstSubGrade = scan.nextDouble();

        System.out.print("Second Subject Grade: ");
        double secondSubGrade = scan.nextDouble();

        System.out.print("Third Subject Grade: ");
        double thirdSubGrade = scan.nextDouble();

        double Average = (firstSubGrade + secondSubGrade + thirdSubGrade) / 3;

        int wholeAverage = (int) Average;

        System.out.println("----------------------");
        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + wholeAverage);
    }
}
