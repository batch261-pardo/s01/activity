package com.zuitt.example;

import java.util.Scanner;
public class UserInput {
    public static void main(String[] args){
        //Scanner is used for obtaining input from the terminal.
        //"System.in" allows us to take input from the console.
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Username: ");

        // To capture the input given by the user, we use the nextLine() method
        String userName = scan.nextLine();
        System.out.println("Hello, " + userName + "!");
    }
}
